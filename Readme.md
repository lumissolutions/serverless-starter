# Serverless Starter

This is a starter project for serverless, express.js and
TypeScript

Go to the api folder and run `npm install`.

To do development run `npm run dev`.  The process will start the
express server.  When the files are written, the process will
recompile them and re-serve them.

To build the bundle that the lambda will actually execute, run `npm run build`.

To test that bundle locally run `npm run offline`.

